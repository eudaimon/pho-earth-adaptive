#! /bin/bash

THEMES_DIR=color_themes
CSS_LINK_NAME=theme_colors.css

maxParams=1

usage() {

        echo -e "Usage: $(basename "$0") [color_theme_to_activate.css]\n" >&2
        echo -e "Activates a color theme\nIf you don't specify one in the command, it will show all possible themes and let you select one'" >&2
}


if [ $# -gt $maxParams ]
then
    echo -e "\nError: Incorrect usage." >&2
    usage
    exit 1
fi


# if we have one parameter, let this be aour destination file
if [ $# -eq 1 ]
then
        destFile=$1 
fi



if [ -z $destFile ]
then
        echo -e "\nList of themes to chose from:\n"
        ls $THEMES_DIR
        echo -e "\nPlease write the name of the theme to use (full name with extension): "
        read destFile
fi


if [[ ! -f $THEMES_DIR/$destFile ]]
then
        echo -e "Error: specified theme does not exist."
        exit 2
fi

#remove link if it exists:
[[ -f $CSS_LINK_NAME ]] && rm $CSS_LINK_NAME

ln -s $THEMES_DIR/$destFile $CSS_LINK_NAME

echo -e "\nSuccessfully created $CSS_LINK_NAME linking to $THEMES_DIR/$destFile\n\n"
