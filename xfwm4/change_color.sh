#! /bin/bash

#Edits all xpm files in current directory, replacing the contents of variable "or" for the contents of "dest"

or="C0C0FF s inactive_color_1"
dest="C0C0FF s inactive_color_2"
files=*inactive.xpm
files_string='*inactive.xpm'
echo "'$or' will be replaced by '$dest' on all $files_string files"
read -p "Are you sure? (Y/N) " resposta
if [[ $resposta == "Y" || $resposta == "y" ]]
then
	echo "will crawl the follwing files:" *inactive.xpm
	sed -i "s/$or/$dest/g" *.xpm
else
    echo "Your answer was not Y, so no doing anything..."
fi


